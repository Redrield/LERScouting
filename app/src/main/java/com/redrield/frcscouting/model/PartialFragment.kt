package com.redrield.frcscouting.model

import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import com.redrield.frcscouting.FormElement
import com.redrield.frcscouting.elements.*
import com.redrield.frcscouting.util.extensions.eq
import kotlinx.android.parcel.Parcelize

enum class FragmentType {
    CHECK_BOX,
    SPINNER,
    EDIT_TEXT,
    FIELD_DIAGRAM,
    TEXT_AREA,
    SUBMIT_BUTTON;
    //TODO

    fun toProtobuf(): FormElement.Type = when (this) {
        FragmentType.CHECK_BOX -> FormElement.Type.CHECKBOX
        FragmentType.SPINNER -> FormElement.Type.SPINNER
        FragmentType.EDIT_TEXT -> FormElement.Type.TEXT_BOX
        FragmentType.SUBMIT_BUTTON -> FormElement.Type.SUBMIT_BUTTON
        FragmentType.FIELD_DIAGRAM -> FormElement.Type.FIELD_DIAGRAM
        FragmentType.TEXT_AREA -> FormElement.Type.TEXT_AREA
    }

    companion object {
        fun fromProtobuf(type: FormElement.Type): FragmentType = when (type) {
            FormElement.Type.SUBMIT_BUTTON -> FragmentType.SUBMIT_BUTTON
            FormElement.Type.TEXT_BOX -> FragmentType.EDIT_TEXT
            FormElement.Type.CHECKBOX -> FragmentType.CHECK_BOX
            FormElement.Type.SPINNER -> FragmentType.SPINNER
            FormElement.Type.FIELD_DIAGRAM -> FragmentType.FIELD_DIAGRAM
            FormElement.Type.TEXT_AREA -> FragmentType.TEXT_AREA
            else -> throw IllegalArgumentException("Unrecognized type $type")
        }
    }
}

/**
 * Represents a serializable version of a form element fragment
 *
 * [type] is the type of the fragment, set by [com.redrield.frcscouting.elements.mutable.MutableFormElementFragment.toImmutable]
 * [args] is the [Bundle] to pass to the [Fragment] upon creation as arguments
 */
@Parcelize
data class PartialFragment(val type: FragmentType, val args: Bundle) : Parcelable {
    /**
     * Creates an Android [Fragment] out of the data we've been provided
     */
    fun compile(): Fragment {
        return when (type) {
            FragmentType.CHECK_BOX -> FormElementCheckboxFragment().apply {
                arguments = args
            }
            FragmentType.SPINNER -> FormElementSpinnerFragment().apply {
                arguments = args
            }
            FragmentType.EDIT_TEXT -> FormElementEditTextFragment().apply {
                arguments = args
            }
            FragmentType.TEXT_AREA -> FormElementTextAreaFragment().apply {
                arguments = args
            }
            FragmentType.SUBMIT_BUTTON -> FormElementSubmitButtonFragment() // No args
            FragmentType.FIELD_DIAGRAM -> FormElementDrawingFragment()
            else -> TODO()
        }
    }

    override fun equals(other: Any?): Boolean {
        return other is PartialFragment && other.type == this.type && other.args eq this.args
    }
}
