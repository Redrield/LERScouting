package com.redrield.frcscouting

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.redrield.frcscouting.elements.FormElementDrawingFragment
import com.redrield.frcscouting.elements.FormElementSubmitButtonFragment
import com.redrield.frcscouting.elements.mutable.*
import com.redrield.frcscouting.model.FragmentType
import com.redrield.frcscouting.model.PartialFragment
import com.redrield.frcscouting.util.extensions.whenSelectionChanged
import kotlinx.android.synthetic.main.activity_new_form_element.*
import kotlinx.android.synthetic.main.activity_new_form_element.view.*

/**
 * Activity for adding a new form element to [MainFragment]
 *
 * Started from [MainFragment]
 */
class NewFormElementActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_form_element)

        // Sets click listeners for our buttons
        buttonContainer.apply {
            confirmButton.setOnClickListener {
                Log.d(TAG, "Confirm button pressed")
                val fragment = supportFragmentManager.findFragmentById(R.id.elementFragment)
                val partial = when(fragment) {
                    is MutableFormElementFragment -> fragment.toImmutable()
                    is FormElementSubmitButtonFragment -> PartialFragment(FragmentType.SUBMIT_BUTTON, Bundle.EMPTY)
                    is FormElementDrawingFragment -> PartialFragment(FragmentType.FIELD_DIAGRAM, Bundle.EMPTY)
                    else -> throw IllegalStateException()
                }

                val intent = Intent().apply {
                    putExtra("newFragment", partial)
                }
                setResult(Activity.RESULT_OK, intent)
                finish()
            }

            cancelButton.setOnClickListener {
                Log.d(TAG, "Cancel button pressed")
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        }

        // Sets fragment transitions when the spinner value changes
        viewTypeSpinner.whenSelectionChanged {
            when (it) {
                "Check Box" -> supportFragmentManager.beginTransaction()
                        .replace(R.id.elementFragment, MutableFormElementCheckboxFragment())
                        .commit()
                "Dropdown Menu" -> supportFragmentManager.beginTransaction()
                    .replace(R.id.elementFragment, MutableFormElementSpinnerFragment())
                    .commit()
                "Text Box" -> supportFragmentManager.beginTransaction()
                    .replace(R.id.elementFragment, MutableFormElementEditTextFragment())
                    .commit()
                "Submit Button" -> supportFragmentManager.beginTransaction()
                    .replace(R.id.elementFragment, FormElementSubmitButtonFragment())
                    .commit()
                "Field Diagram" -> supportFragmentManager.beginTransaction()
                    .replace(R.id.elementFragment, FormElementDrawingFragment())
                    .commit()
                "Text Area" -> supportFragmentManager.beginTransaction()
                        .replace(R.id.elementFragment, MutableFormElementTextAreaFragment())
                        .commit()
                else -> TODO()
            }
        }

        supportFragmentManager.beginTransaction()
            .add(R.id.elementFragment, MutableFormElementCheckboxFragment())
            .commit()
    }

    companion object {
        val TAG = NewFormElementActivity::class.java.name
    }
}
