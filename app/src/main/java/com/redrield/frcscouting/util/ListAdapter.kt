package com.redrield.frcscouting.util

import android.content.Context
import android.support.annotation.LayoutRes
import android.widget.ArrayAdapter

/**
 * Wrapper around [ArrayAdapter] that keeps track of items
 */
class ListAdapter<T>(ctx: Context, @LayoutRes layout: Int) : ArrayAdapter<T>(ctx, layout) {
    val items = mutableListOf<T>()

    override fun add(item: T) {
        super.add(item)
        items.add(item)
    }

    override fun remove(item: T) {
        super.remove(item)
        items.remove(item)
    }
}
