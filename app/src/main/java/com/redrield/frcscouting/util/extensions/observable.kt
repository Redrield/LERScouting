package com.redrield.frcscouting.util.extensions

import io.reactivex.Observable
import java.util.concurrent.TimeUnit

data class Time(val amount: Long, val unit: TimeUnit)

val Number.ms
    get() = Time(this.toLong(), TimeUnit.MILLISECONDS)

fun <T> Observable<T>.debounce(time: Time) = this.debounce(time.amount, time.unit)