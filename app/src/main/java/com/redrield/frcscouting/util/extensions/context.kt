package com.redrield.frcscouting.util.extensions

import android.content.Context
import java.io.File

val Context.layoutsDir: File
    get() = File(filesDir, "layouts")
