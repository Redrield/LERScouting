package com.redrield.frcscouting.util.extensions

fun <E> List<List<E>>.flattenWithSeparator(joiner: E): List<E> = reduce { acc, col -> acc + joiner + col }

fun <E> MutableList<E>.removeAtOrNull(idx: Int): E? {
    return if(lastIndex < idx) {
        null
    }else {
        removeAt(idx)
    }
}