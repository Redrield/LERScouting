package com.redrield.frcscouting.util

import io.reactivex.subjects.PublishSubject

/**
 * Wrapper around [MutableList] that also has an Rx [PublishSubject] so that I can do reactive programming
 */
class ObservableList<T>(private val l: MutableList<T>, private val publisher: PublishSubject<T>) : MutableList<T> by l {

    constructor() : this(ArrayList<T>(), PublishSubject.create())

    override fun add(element: T): Boolean {
        publisher.onNext(element)
        return l.add(element)
    }

    override fun add(index: Int, element: T) {
        l.add(index, element)
        publisher.onNext(element)
    }

    override fun addAll(elements: Collection<T>): Boolean {
        elements.forEach(publisher::onNext)
        return l.addAll(elements)
    }

    override fun addAll(index: Int, elements: Collection<T>): Boolean {
        elements.forEach(publisher::onNext)
        return l.addAll(index, elements)
    }

    fun subscribe(consumer: (T) -> Unit) {
        publisher.subscribe(consumer)
    }

}