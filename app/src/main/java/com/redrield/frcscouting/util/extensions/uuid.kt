package com.redrield.frcscouting.util.extensions

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*

fun UUID.endianSwap(newEndian: ByteOrder = ByteOrder.LITTLE_ENDIAN): UUID {
    val buf = ByteBuffer.allocate(16)
    buf.putLong(leastSignificantBits)
    buf.putLong(mostSignificantBits)

    buf.rewind()
    buf.order(newEndian)

    return UUID(buf.long, buf.long)
}