package com.redrield.frcscouting.util.extensions

import android.content.IntentFilter

fun intentFilterOf(vararg actions: String): IntentFilter = IntentFilter().apply {
    actions.forEach(this::addAction)
}