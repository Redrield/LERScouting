package com.redrield.frcscouting.util.extensions

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner

/**
 * Ergonomic way to bind a closure to the change event for a spinner (dropdown)
 */
inline fun Spinner.whenSelectionChanged(crossinline block: (String) -> Unit) {
    onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            block(this@whenSelectionChanged.getItemAtPosition(position).toString())
        }
    }
}