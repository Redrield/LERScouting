package com.redrield.frcscouting.util.extensions

import android.os.Bundle

infix fun Bundle.eq(other: Bundle): Boolean {
    if(this == other) {
        return true
    }

    for(key in this.keySet()) {
        val value = this[key]
        if(other[key] != value) {
            return false
        }
    }

    return true
}