package com.redrield.frcscouting.util.extensions

import android.os.Bundle
import com.google.protobuf.Any
import com.google.protobuf.Int32Value
import com.google.protobuf.Int64Value
import com.google.protobuf.ListValue
import com.google.protobuf.StringValue
import com.google.protobuf.Value

fun Bundle.toProtobufMap(): Map<String, Any> {
    val map = mutableMapOf<String, Any>()

    for (key in keySet()) {
        val value = this[key]

        when (value) {
            is String -> map[key] = Any.pack(StringValue
                    .newBuilder()
                    .setValue(value)
                    .build())
            is Int -> map[key] = Any.pack(Int32Value.newBuilder()
                    .setValue(value)
                    .build())
            is Long -> Any.pack(Int64Value.newBuilder()
                    .setValue(value)
                    .build())
            is ArrayList<*> -> {
                val first = value.firstOrNull()
                when (first) {
                    is String -> {
                        val values = (value as ArrayList<String>).map {
                            Value.newBuilder()
                                    .setStringValue(it)
                                    .build()
                        }

                        map[key] = Any.pack(ListValue
                                .newBuilder()
                                .addAllValues(values)
                                .build())
                    }
                    is Int -> {
                        val values = (value as ArrayList<Int>).map {
                            Value.newBuilder()
                                    .setNumberValue(it.toDouble())
                                    .build()
                        }

                        map[key] = Any.pack(ListValue
                                .newBuilder()
                                .addAllValues(values)
                                .build())
                    }
                }
            }
        }
    }

    return map.toMap()
}

fun Map<String, kotlin.Any>.toBundle(): Bundle {
    val bundle = Bundle()

    for ((key, value) in this.entries) {
        bundle.apply {
            putProtobuf(key, value)
            putNormal(key, value)

        }
    }

    return bundle
}

fun Bundle.putNormal(key: String, value: kotlin.Any) {
    when(value) {
        is String -> putString(key, value)
        is Int -> putInt(key, value)
        is Long -> putLong(key, value)
        is ArrayList<*> -> when(value.firstOrNull()) {
            is String -> putStringArrayList(key, value as ArrayList<String>)
            is Int -> putIntegerArrayList(key, value as ArrayList<Int>)
        }
    }
}

fun Bundle.putProtobuf(key: String, value: kotlin.Any) {
    if(value is Any) {
        when {
            value.`is`(StringValue::class.java) -> putString(key, value.unpack(StringValue::class.java).value)
            value.`is`(Int32Value::class.java) -> putInt(key, value.unpack(Int32Value::class.java).value)
            value.`is`(Int64Value::class.java) -> putLong(key, value.unpack(Int64Value::class.java).value)
            value.`is`(ListValue::class.java) -> {
                val values = value.unpack(ListValue::class.java)
                val first = values.getValues(0)

                when {
                    first.stringValue != null -> {
                        val stringArray = values.valuesList
                                .map(Value::getStringValue)
                        putStringArrayList(key, ArrayList(stringArray))
                    }
                    first.numberValue != 0.0 -> {
                        val numArray = values.valuesList
                                .asSequence()
                                .map(Value::getNumberValue)
                                .map(Double::toInt)
                                .toList()
                        putIntegerArrayList(key, ArrayList(numArray))
                    }
                }
            }
        }
    }
}