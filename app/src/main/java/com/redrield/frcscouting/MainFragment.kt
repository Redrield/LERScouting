package com.redrield.frcscouting

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Spinner
import com.google.firebase.firestore.FirebaseFirestore
import com.redrield.frcscouting.async.result
import com.redrield.frcscouting.elements.FormElementChangeListener
import com.redrield.frcscouting.elements.FormElementDrawingFragment
import com.redrield.frcscouting.elements.FormElementMessage
import com.redrield.frcscouting.model.FragmentType
import com.redrield.frcscouting.model.PartialFragment
import com.redrield.frcscouting.util.FileDialog
import com.redrield.frcscouting.util.ObservableList
import com.redrield.frcscouting.util.extensions.layoutsDir
import com.redrield.frcscouting.util.extensions.toBundle
import com.redrield.frcscouting.util.extensions.toProtobufMap
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.centerInParent
import org.jetbrains.anko.childrenRecursiveSequence
import org.jetbrains.anko.customView
import org.jetbrains.anko.dip
import org.jetbrains.anko.editText
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.noButton
import org.jetbrains.anko.padding
import org.jetbrains.anko.relativeLayout
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivityForResult
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.yesButton
import java.io.ByteArrayOutputStream
import java.io.File

class MainFragment : Fragment(), FormElementChangeListener {

    val elements = ObservableList<PartialFragment>()
    private val database by lazy {
        FirebaseFirestore.getInstance()
    }
    var data = mutableMapOf<String, Any>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Bind a subscriber to elements to add the fragment to the view
        elements.subscribe {
            childFragmentManager
                    .beginTransaction()
                    .add(R.id.formList, it.compile())
                    .commit()
        }

        // If we saved some state, load the specified layout
        val loadedLayout = activity?.getPreferences(Context.MODE_PRIVATE)
                ?.getString("loadedLayout", "")
        if (loadedLayout != "") {
            loadLayoutFile(File(ctx.layoutsDir, "$loadedLayout.pb"))
        }

        Log.d(TAG, "Elements is ${elements.toList()}")
        val unenteredViews = savedInstanceState?.getParcelableArrayList<PartialFragment>("elements")
                ?.filterNot { elements.toList().contains(it) }

        if (elements.isEmpty()) {
            clearLayout()
        }

        unenteredViews?.forEach { elements.add(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_main_toolbar, menu)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList("elements", ArrayList(elements))
    }

    /**
     * Called when an item in the options menu is clicked
     */
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.add_view_item -> {
            addViewItem()
            true
        }
        R.id.save_layout_item -> {
            saveCurrentLayout()
            true
        }
        R.id.load_layout_item -> {
            loadLayout()
            true
        }
        R.id.new_layout_item -> {
            clearLayout()
            true
        }
        R.id.delete_layout_item -> {
            deleteLayout()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    /**
     * Called when an activity returns with a result
     *
     * Used in this circumstance to return the data from [NewFormElementActivity] and add a fragment
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == NEW_FORM_ELEMENT_CODE && resultCode == Activity.RESULT_OK) {
            val newFragment = data!!.getParcelableExtra<PartialFragment>("newFragment")
            elements.add(newFragment)
        }
    }

    /**
     * Called when any of our fragments' UI gets updated. Change final database payload here
     */
    override fun onMessage(msg: FormElementMessage) {
        when (msg) {
            is FormElementMessage.EditText -> data[msg.label] = msg.newText
            is FormElementMessage.CheckBox -> data[msg.label] = msg.checked
            is FormElementMessage.Spinner -> data[msg.label] = msg.selection
            is FormElementMessage.ReqDisallowScroll -> formScrollView.requestDisallowInterceptTouchEvent(true)
            is FormElementMessage.ReqAllowScroll -> formScrollView.requestDisallowInterceptTouchEvent(false)
            is FormElementMessage.FieldDiagram -> {
                val bmp = msg.bmp
                val buf = ByteArrayOutputStream()
                bmp.compress(Bitmap.CompressFormat.JPEG, 25, buf)
                val enc = Base64.encodeToString(buf.toByteArray(), Base64.DEFAULT)
                Log.d(TAG, "enc length is ${enc.toByteArray().size}")
                data["Autonomous Path"] = enc
            }
            is FormElementMessage.Submit -> {

                launch(UI) {

                    Log.d(TAG, "Data as written ${data.toMap()}")

                    // Write what we have to the local db and reset the data
                    launch {
                        try {
                            database.collection("scouting-data")
                                    .add(data)
                                    .result()


                            data.clear()
                        }catch(_: Exception) {
                            toast("Error uploading data.")
                        }
                    }

                    val fragment = childFragmentManager.fragments.asSequence().filterIsInstance<FormElementDrawingFragment>().singleOrNull()
                    fragment?.clearView()

                    childFragmentManager.fragments
                            .asSequence()
                            .mapNotNull(Fragment::getView)
                            .flatMap(View::childrenRecursiveSequence)
                            .forEach { child ->
                                when (child) {
                                    is EditText -> child.setText("")
                                    is CheckBox -> child.isChecked = false
                                    is Spinner -> child.setSelection(0)
                                }
                            }
                }
            }
        }
    }

    /**
     * Starts the activity to add items to our form view
     */
    private fun addViewItem() {
        // ForResult means that onActivityResult will be called when this terminates
        startActivityForResult<NewFormElementActivity>(NEW_FORM_ELEMENT_CODE)
    }

    /**
     * Saves the layout as stored into JSON under our [layoutsDir]
     */
    private fun saveCurrentLayout() {
        alert {
            lateinit var newItem: EditText
            customView {
                relativeLayout {
                    newItem = editText {
                        hint = "Name"
                    }.lparams(width = matchParent) {
                        centerInParent()
                        padding = dip(10)
                    }
                }
            }

            yesButton {
                if (newItem.text.isNotEmpty()) {
                    saveLayoutFile(newItem.text.toString())
                }
            }
        }.show()
    }

    /**
     * Loads a layout from disk, file specified by the user
     */
    private fun loadLayout() {
        val dialog = FileDialog(activity, ctx.layoutsDir, ".pb", "Layouts")
        dialog.addFileListener {
            loadLayoutFile(it)
        }
        dialog.showDialog()
    }

    private fun deleteLayout() {
        val dialog = FileDialog(activity, ctx.layoutsDir, ".pb", "Layouts")
        dialog.addFileListener {
            alert {
                message = "Are you sure you want to delete ${it.name}?"

                yesButton { _ ->
                    it.delete()
                }

                noButton { }
            }.show()
        }
        dialog.showDialog()
    }

    /**
     * Saves a file with the given [name]
     *
     * Stored as a list of [PartialFragment] serialized into JSON
     */
    private fun saveLayoutFile(name: String) {
        val elements = this.elements
                .asSequence()
                .map {
                    FormElement
                            .newBuilder()
                            .setType(it.type.toProtobuf())
                            .putAllArgs(it.args.toProtobufMap())
                            .build()
                }
                .toList()

        val payload = Payload
                .newBuilder()
                .addAllElements(elements)
                .build()

        val file = File(ctx.layoutsDir, "$name.pb")
        payload.writeTo(file.outputStream())
    }

    /**
     * Loads a layout from the given [file]
     *
     * Deserializes the JSON from the file, and sets shared preferences loadedLayout to save state
     */
    private fun loadLayoutFile(file: File) {
        clearLayout()
        val payload = Payload.parseFrom(file.inputStream())
        val elements = payload.elementsList
                .map {
                    PartialFragment(
                            type = FragmentType.fromProtobuf(it.type),
                            args = it.argsMap.toBundle()
                    )
                }

        activity?.getPreferences(Context.MODE_PRIVATE)?.edit()
                ?.putString("loadedLayout", file.nameWithoutExtension)
                ?.apply()

        this.elements += elements
    }

    /**
     * Clears the layout as shown to the users, clears [elements], and resets the loadedLayout state
     */
    private fun clearLayout() {
        // Unset the loaded layout
        activity?.getPreferences(Context.MODE_PRIVATE)?.edit()
                ?.putString("loadedLayout", "")
                ?.apply()

        elements.clear()
        // This activity only has fragments in the form of form elements, so get all the fragments and get rid of them all
        childFragmentManager.beginTransaction().apply {
            childFragmentManager
                    .fragments
                    .filterNotNull()
                    .forEach {
                        remove(it)
                    }
        }
                .commit()
    }

    companion object {
        val TAG = MainFragment::class.java.name

        // Request code for a new element. Used to identify in onActivityResult
        const val NEW_FORM_ELEMENT_CODE = 1
    }
}
