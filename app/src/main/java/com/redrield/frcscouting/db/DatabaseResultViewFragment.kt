package com.redrield.frcscouting.db

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.redrield.frcscouting.R
import com.redrield.frcscouting.async.result
import com.redrield.frcscouting.elements.FormElementFragment
import kotlinx.android.synthetic.main.fragment_database_view.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [DatabaseResultViewFragment.OnListFragmentInteractionListener] interface.
 */
class DatabaseResultViewFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_database_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        launch(UI) {
            val pagerData = withContext(CommonPool) {
                FirebaseFirestore.getInstance()
                        .collection("scouting-data")
                        .get()
                        .result()
                        .documents
                        .map(DocumentSnapshot::getData)
                        .map { DatabaseResult(it) }
            }
                    .map { it.cards }

            viewContainer.adapter = SectionAdapter(pagerData)
        }
//        childFragmentManager.beginTransaction().apply {
//            launch(UI) {
//                withContext(CommonPool) {
//                    FirebaseFirestore.getInstance()
//                            .collection("scouting-data")
//                            .get()
//                            .result()
//                            .documents
//                            .map(DocumentSnapshot::getData)
//                            .map { DatabaseResult(it) }
//                }
//                        .map { it.cards }
//                        .flattenWithSeparator(FormElementSeparator())
//                        .forEach {
//                            println(it)
//                            add(R.id.database_results, it)
//                        }
//
//                commit()
//            }
//        }
    }

    inner class SectionAdapter(val data: List<List<FormElementFragment>>) : FragmentPagerAdapter(childFragmentManager) {
        override fun getItem(position: Int): Fragment {
            return DatabaseResultFragment().apply {
                results = data[position]
            }
        }

        override fun getCount(): Int {
            return data.size
        }

    }
}
