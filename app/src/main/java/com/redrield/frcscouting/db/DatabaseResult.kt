package com.redrield.frcscouting.db

import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.util.Base64
import com.redrield.frcscouting.elements.FormElementCheckboxFragment
import com.redrield.frcscouting.elements.FormElementDrawingFragment
import com.redrield.frcscouting.elements.FormElementEditTextFragment
import com.redrield.frcscouting.elements.PARAM_CHECKBOX_LABEL
import kotlinx.android.synthetic.main.fragment_form_element_checkbox.view.*
import kotlinx.android.synthetic.main.fragment_form_element_drawing.view.*
import kotlinx.android.synthetic.main.fragment_form_element_edit_text.view.*

class DatabaseResult(results: Map<String, Any>) {
    val cards = results.map { (k, v) ->
        if(k == "Autonomous Path") {
            return@map FormElementDrawingFragment().apply {
                val imgBuf = Base64.decode(v as String, 0)
                uiCallback {
                    it.clearImageButton.isEnabled = false
                    bmp = BitmapFactory.decodeByteArray(imgBuf, 0, imgBuf.size)
                    viewListener.dispose()
                }
            }
        }

        when(v) {
            is String -> FormElementEditTextFragment().apply {
                uiCallback {
                    it.editText.text = Editable.Factory.getInstance().newEditable(v)
                    it.editText.isEnabled = false
                }

                arguments = Bundle().apply {
                    putString(FormElementEditTextFragment.EDIT_TEXT_LABEL, k)
                    putInt(FormElementEditTextFragment.EDIT_TEXT_TYPE, InputType.TYPE_CLASS_TEXT)
                }
            }
            is Int -> FormElementEditTextFragment().apply {
                uiCallback {
                    it.editText.text = Editable.Factory.getInstance().newEditable(v.toString())
                    it.editText.isEnabled = false
                }

                arguments = Bundle().apply {
                    putString(FormElementEditTextFragment.EDIT_TEXT_LABEL, k)
                    putInt(FormElementEditTextFragment.EDIT_TEXT_TYPE, InputType.TYPE_CLASS_NUMBER)
                }
            }
            is Boolean -> FormElementCheckboxFragment().apply {
                uiCallback {
                    it.checkBox.isEnabled = false
                    it.checkBox.isChecked = v
                }

                arguments = Bundle().apply {
                    putString(PARAM_CHECKBOX_LABEL, k)
                }
            }
            else -> throw IllegalArgumentException()
        }
    }
}
