package com.redrield.frcscouting.db

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.redrield.frcscouting.R
import com.redrield.frcscouting.elements.FormElementFragment

class DatabaseResultFragment : Fragment() {
    var results: List<FormElementFragment>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_database_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        childFragmentManager.beginTransaction().apply {
            // Fragments aren't serializable, so the only way to fill `results` is to fill it after construction (no arguments, no constructor)
            // If it's still null at this point, something is already wrong and there's no point in trying to be null safe.
            results!!.forEach {
                add(R.id.resultContainer, it)
            }

            commit()
        }
    }
}