package com.redrield.frcscouting

import android.content.Context
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.redrield.frcscouting.comm.FileShareFragment
import com.redrield.frcscouting.db.DatabaseResultViewFragment
import com.redrield.frcscouting.util.extensions.layoutsDir
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val toggle = object : ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            override fun onDrawerOpened(drawerView: View) {
                imm.hideSoftInputFromWindow(drawerView.windowToken, 0)
            }
        }
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()


        nav_view.setNavigationItemSelectedListener(this)

        if (!layoutsDir.exists()) {
            layoutsDir.mkdirs()
        }

        supportFragmentManager
            .beginTransaction()
            .add(R.id.rootFragmentContainer, MainFragment())
            .commit()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.home_item -> supportFragmentManager
                .beginTransaction()
                .replace(R.id.rootFragmentContainer, MainFragment())
                .commit()
            R.id.share_layout_item -> supportFragmentManager
                .beginTransaction()
                .replace(R.id.rootFragmentContainer, FileShareFragment())
                .commit()
            R.id.database_results_item -> supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.rootFragmentContainer, DatabaseResultViewFragment())
                    .commit()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
