package com.redrield.frcscouting.comm

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.redrield.frcscouting.Payload
import com.redrield.frcscouting.R
import com.redrield.frcscouting.util.FileDialog
import com.redrield.frcscouting.util.ObservableList
import com.redrield.frcscouting.util.extensions.intentFilterOf
import com.redrield.frcscouting.util.extensions.layoutsDir
import kotlinx.android.synthetic.main.fragment_file_share.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.cancelAndJoin
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import org.jetbrains.anko.alert
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.yesButton
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class FileShareFragment : Fragment() {

    lateinit var btAdapter: BluetoothAdapter
    lateinit var viewAdapter: PeerListAdapter

    val pendingDevices = mutableListOf<BluetoothDevice>()

    private val scanReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                BluetoothDevice.ACTION_FOUND -> {
                    val dev = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                    Log.d(TAG, "Got device ${dev.name} with address ${dev.address}")

                    if(!pendingDevices.contains(dev)) {
                        pendingDevices.add(dev)
                    }
                }

                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                    Log.d(TAG, "Discovery finished, querying UUIDs")

                    if(pendingDevices.isNotEmpty()) {
                        val filter = intentFilterOf(BluetoothDevice.ACTION_UUID)
                        activity!!.registerReceiver(ServiceQueryReceiver(this@FileShareFragment, ArrayList(pendingDevices)), filter)
                    }else {
                        peerProgressBar.visibility = View.INVISIBLE
                    }
                }

                BluetoothAdapter.ACTION_DISCOVERY_STARTED -> {
                    Log.d(TAG, "Discovery started")
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val filter = intentFilterOf(BluetoothDevice.ACTION_FOUND,
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED,
                BluetoothAdapter.ACTION_DISCOVERY_STARTED)
        activity!!.registerReceiver(scanReceiver, filter)

        setupBluetoothWithPermissionCheck()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_share_toolbar, menu)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_file_share, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewAdapter = PeerListAdapter(this, ObservableList())
        viewAdapter.dataset.subscribe {
            peerProgressBar.visibility = View.INVISIBLE
        }
        recyclerView.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(activity)
            adapter = viewAdapter
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.begin_receive_files -> {
            runServer()
            true
        }
        R.id.reload_peer_view -> {
            reloadRecycler()
            true
        }
        else -> false
    }

    override fun onDestroy() {
        super.onDestroy()
        if (btAdapter.isDiscovering) {
            btAdapter.cancelDiscovery()
        }
        activity!!.unregisterReceiver(scanReceiver)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQ_ENABLE_BT -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        btAdapter.startDiscovery()
                    }
                    Activity.RESULT_CANCELED -> {
                        ctx.alert {
                            message = "Cannot share file without bluetooth"

                            yesButton {

                            }
                        }.show()
                    }
                }
            }

            REQ_ENABLE_DISCOV -> runServer()
        }
    }

    private fun reloadRecycler() {
        if(btAdapter.isDiscovering) {
            btAdapter.cancelDiscovery()
        }

        viewAdapter.dataset.clear()
        viewAdapter.notifyDataSetChanged()
        pendingDevices.clear()
        btAdapter.startDiscovery()
        btAdapter.startDiscovery()
        peerProgressBar.visibility = View.VISIBLE
    }

    private fun runServer() {
        if (btAdapter.scanMode != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            val intent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0)
            startActivityForResult(intent, REQ_ENABLE_DISCOV)
            return
        }

        btAdapter.cancelDiscovery()

        val thread = launch {
            val server = FileShareService(btAdapter, ctx)

            while (isActive) {
                server.run()
            }
        }

        ctx.alert {
            message = "Click to stop receiving files"
            onCancelled {
                launch {
                    thread.cancelAndJoin()
                }
            }

            yesButton {
                launch {
                    thread.cancelAndJoin()
                }
            }
        }.show()
    }

    /**
     * Ran when an item in the recycler view is clicked
     *
     * Attempts to open a connection to the device, and sends the selected layout
     */
    fun onItemClicked(dev: BluetoothDevice) {
        launch(UI) {
            withContext(CommonPool) {
                btAdapter.cancelDiscovery()
            }

            val sock = withContext(CommonPool) {
                val conn = dev.createRfcommSocketToServiceRecord(FileShareService.SERVICE_UUID)
                conn.connect()

                conn
            }

            val ret = withContext(CommonPool) {
                sock.outputStream.write(byteArrayOf(0x40, 0x69))
                sock.outputStream.flush()

                sock.inputStream.read()
            }

            if (ret == 0xFF) {
                val dialog = FileDialog(activity, ctx.layoutsDir, ".pb", "Layouts")
                dialog.addFileListener {
                    launch {
                        // Read the payload from the file, add the file name and send it
                        val payload = Payload.parseFrom(it.inputStream())
                                .toBuilder()
                                .setFileName(it.nameWithoutExtension)
                                .build()

                        payload.writeDelimitedTo(sock.outputStream)
                        sock.inputStream.read()
                        sock.outputStream.write(0x00)
                        sock.outputStream.flush()
                        sock.close()
                    }
                }
                dialog.showDialog()
            } else if (ret == 0xEE) {
                withContext(CommonPool) {
                    sock.close()
                }
            }
        }
    }

    @NeedsPermission(Manifest.permission.BLUETOOTH, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun setupBluetooth() {
        val a = BluetoothAdapter.getDefaultAdapter()
        if (a != null) {
            btAdapter = a
        }

        if (!btAdapter.isEnabled) {
            enableBluetooth()
        } else {
            btAdapter.startDiscovery()
        }
    }

    private fun enableBluetooth() {
        startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), REQ_ENABLE_BT)
    }

    companion object {
        val TAG = FileShareFragment::class.java.name

        const val REQ_ENABLE_BT = 1
        const val REQ_ENABLE_DISCOV = 2
    }
}
