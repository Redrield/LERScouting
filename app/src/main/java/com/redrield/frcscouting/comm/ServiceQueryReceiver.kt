package com.redrield.frcscouting.comm

import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.ParcelUuid
import android.view.View
import com.redrield.frcscouting.util.extensions.endianSwap
import com.redrield.frcscouting.util.extensions.removeAtOrNull
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_file_share.*

class ServiceQueryReceiver(val parent: FileShareFragment, val pendingDevices: MutableList<BluetoothDevice>) : BroadcastReceiver() {
    val subject = PublishSubject.create<BluetoothDevice>()

    init {
        // Initialize listeners on PublishSubject
        subject
                .doOnComplete {
                    parent.peerProgressBar.visibility = View.INVISIBLE
                    parent.activity!!.unregisterReceiver(this)
                }
                .subscribe {
                    val uuids = it.uuids?.map(ParcelUuid::getUuid)
                            ?.zip(it.uuids?.map(ParcelUuid::getUuid)?.map { it.endianSwap() } ?: listOf()) ?: listOf()
                    if (uuids.any { (u1, u2) -> u1 == FileShareService.SERVICE_UUID || u2 == FileShareService.SERVICE_UUID }) {
                        addToDataset(it)
                    } else {
                        it.fetchUuidsWithSdp()
                    }
                }

        putNext()
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == BluetoothDevice.ACTION_UUID) {
            val parcels = intent.getParcelableArrayExtra(BluetoothDevice.EXTRA_UUID)
            val ids = mutableListOf<ParcelUuid>()
            if (parcels != null && parcels.isNotEmpty()) {
                parcels.mapTo(ids) { it as ParcelUuid }
            }
            // Stupid workaround due to some bug in SDP that gives me endian mismatched UUIDs
            if (ids.any { it.uuid == FileShareService.SERVICE_UUID || it.uuid == FileShareService.SERVICE_UUID.endianSwap() }) {
                val dev = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                if (!parent.viewAdapter.dataset.any { it.name == dev.name }) {

                    addToDataset(dev)
                }
            }

            putNext()
        }
    }

    private fun addToDataset(dev: BluetoothDevice) {
        parent.viewAdapter.dataset.add(dev)
        parent.viewAdapter.notifyDataSetChanged()
    }

    /**
     * Function to advance the state of the PublishSubject.
     *
     * If there are still items in [pendingDevices], publishes the head with [PublishSubject.onNext]
     * If there are no items left, calls [PublishSubject.onComplete], which terminates all processes and unregisters this receiver
     */
    private fun putNext() {

        val next = pendingDevices.removeAtOrNull(0)

        if (next != null) {
            subject.onNext(next)
        } else {
            subject.onComplete()
        }
    }
}