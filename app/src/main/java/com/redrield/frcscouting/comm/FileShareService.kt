package com.redrield.frcscouting.comm

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.util.Log
import com.redrield.frcscouting.Payload
import com.redrield.frcscouting.util.extensions.layoutsDir
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.yesButton
import java.io.File
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*

/**
 * Bluetooth server socket for file transfer
 */
class FileShareService(adapter: BluetoothAdapter, private val ctx: Context) : Runnable {

    private val socket: BluetoothServerSocket

    init {
        Log.d(TAG, "FileShareService initialized. socket opened")
        socket = adapter.listenUsingRfcommWithServiceRecord(NAME, SERVICE_UUID)
    }

    override fun run() {
        // Try to accept a peer and handle it if not null
        val peer = try {
            socket.accept(1000)
        } catch (_: IOException) {
            null
        }

        handlePeer(peer ?: return)
    }

    fun cancel() {
        socket.close()
    }

    private fun handlePeer(peer: BluetoothSocket) {
        // Read the magic number, check equal to 0x4069
        val handshakeBuf = ByteArray(2)
        peer.inputStream.read(handshakeBuf)
        val handshake = ByteBuffer.wrap(handshakeBuf).order(ByteOrder.BIG_ENDIAN)
                .asShortBuffer().get().toInt()

        if (handshake == 0x4069) {
            ctx.runOnUiThread {
                // Get user confirmation and send next packet based on that
                alert {
                    message = "Request received from ${peer.remoteDevice.name}!"

                    yesButton {
                        launch(UI) {
                            val fileName = withContext(CommonPool) {
                                Log.d(TAG, "Accepting request")
                                peer.outputStream.write(0xFF)
                                peer.outputStream.flush()

                                val payload = Payload.parseDelimitedFrom(peer.inputStream)

                                Log.d(TAG, "Got file payload $payload")
                                val out = File(ctx.layoutsDir, "${payload.fileName}.pb")
                                payload.writeTo(out.outputStream())

                                peer.outputStream.write(0x00)
                                peer.outputStream.flush()
                                peer.inputStream.read()
                                peer.close()

                                payload.fileName
                            }

                            alert {
                                message = "Got $fileName.pb from ${peer.remoteDevice.name}"

                                yesButton {  }
                            }.show()
                        }
                    }

                    noButton {
                        launch {
                            peer.outputStream.write(0xEE)
                            peer.outputStream.flush()
                            peer.close()
                        }
                    }
                }.show()
            }
        }
    }

    companion object {
        val TAG = FileShareService::class.java.name

        val SERVICE_UUID: UUID = UUID.fromString("ccc1158c-298f-4e86-8a9c-f1bacc4b04d5")
        const val NAME = "com.redrield.frcscouting.comm.ShareServer"
    }
}
