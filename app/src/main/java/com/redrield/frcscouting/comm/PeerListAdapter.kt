package com.redrield.frcscouting.comm

import android.bluetooth.BluetoothDevice
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.redrield.frcscouting.R
import com.redrield.frcscouting.util.ObservableList
import kotlinx.android.synthetic.main.peer_card_view.view.*

class PeerListAdapter(val parent: FileShareFragment, val dataset: ObservableList<BluetoothDevice>) : RecyclerView.Adapter<PeerListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.peer_card_view, parent, false) as CardView
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = dataset.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.peerCard.nameTextView.text = dataset[position].name
        holder.peerCard.setOnClickListener {
            parent.onItemClicked(dataset[position])
        }
    }

    inner class ViewHolder(val peerCard: CardView) : RecyclerView.ViewHolder(peerCard)

}