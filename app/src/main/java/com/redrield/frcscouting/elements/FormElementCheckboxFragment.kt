package com.redrield.frcscouting.elements

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.redrield.frcscouting.R
import kotlinx.android.synthetic.main.fragment_form_element_checkbox.*

const val PARAM_CHECKBOX_LABEL = "CHECKBOX_LABEL"

class FormElementCheckboxFragment : FormElementFragment() {

    lateinit var checkboxLabel: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            checkboxLabel = it.getString(PARAM_CHECKBOX_LABEL)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_form_element_checkbox, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkBox.text = this.checkboxLabel

        // Send a default so that it gets included
        listener?.onMessage(FormElementMessage.CheckBox(false, checkboxLabel))

        checkBox.setOnClickListener {
            listener?.onMessage(FormElementMessage.CheckBox(checkBox.isChecked, checkboxLabel))
        }
    }
}
