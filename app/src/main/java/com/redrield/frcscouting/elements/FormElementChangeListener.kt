package com.redrield.frcscouting.elements

import android.graphics.Bitmap

sealed class FormElementMessage {
    data class EditText(val newText: String, val label: String) : FormElementMessage()
    data class CheckBox(val checked: Boolean, val label: String) : FormElementMessage()
    data class Spinner(val selection: String, val label: String) : FormElementMessage()
    data class FieldDiagram(val bmp: Bitmap) : FormElementMessage()

    object ReqDisallowScroll : FormElementMessage() {
        override fun toString() = "ReqDisallowScroll"
    }
    object ReqAllowScroll : FormElementMessage() {
        override fun toString() = "ReqAllowScroll"
    }
    object Submit : FormElementMessage() {
        override fun toString() = "Submit"
    }
}

interface FormElementChangeListener {
    fun onMessage(msg: FormElementMessage)
}