package com.redrield.frcscouting.elements

import android.content.Context
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import android.view.View

abstract class FormElementFragment : Fragment() {
    private var uiCallback: ((View) -> Unit)? = null
    var listener: FormElementChangeListener? = null

    fun uiCallback(block: (View) -> Unit) {
        uiCallback = block
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if(parentFragment is FormElementChangeListener) {
            listener = parentFragment as FormElementChangeListener
        }
    }

    override fun onDetach() {
        super.onDetach()

        listener = null
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        uiCallback?.invoke(view)
    }
}