package com.redrield.frcscouting.elements

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.InputType
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.redrield.frcscouting.R
import kotlinx.android.synthetic.main.fragment_form_element_text_area.*
import kotlinx.android.synthetic.main.fragment_form_element_text_area.view.*
import java.util.concurrent.TimeUnit

class FormElementTextAreaFragment : FormElementFragment() {
    lateinit var label: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_form_element_text_area, container, false)
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.textAreaLabel.text = label
        textArea.movementMethod = ScrollingMovementMethod()
        textArea.isVerticalScrollBarEnabled = true

        textArea.setOnTouchListener { v, event ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            if((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                v.parent.requestDisallowInterceptTouchEvent(false)
            }

            false
        }

        listener?.onMessage(FormElementMessage.EditText("", label))

        RxTextView.textChanges(textArea)
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribe {
                    listener?.onMessage(FormElementMessage.EditText(it.toString(), label))
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            label = it.getString(TEXT_AREA_LABEL)!!
        }
    }

    companion object {
        const val TEXT_AREA_LABEL = "TEXT_AREA_LABEL"
    }
}