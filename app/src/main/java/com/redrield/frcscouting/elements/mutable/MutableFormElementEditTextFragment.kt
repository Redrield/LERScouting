package com.redrield.frcscouting.elements.mutable

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.redrield.frcscouting.R
import com.redrield.frcscouting.elements.FormElementEditTextFragment
import com.redrield.frcscouting.model.FragmentType
import com.redrield.frcscouting.model.PartialFragment
import kotlinx.android.synthetic.main.fragment_mutable_form_element_edit_text.*

class MutableFormElementEditTextFragment : MutableFormElementFragment() {

    override fun toImmutable(): PartialFragment {
        val args = Bundle().apply {
            putString(FormElementEditTextFragment.EDIT_TEXT_LABEL, editTextLabel.text.toString())
            putInt(FormElementEditTextFragment.EDIT_TEXT_TYPE, when(editTextTypeSpinner.selectedItem.toString()) {
                "Text" -> InputType.TYPE_CLASS_TEXT
                "Number" -> InputType.TYPE_CLASS_NUMBER
                else -> TODO()
            })
        }

        return PartialFragment(FragmentType.EDIT_TEXT, args)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_mutable_form_element_edit_text, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        val TAG = MutableFormElementEditTextFragment::class.java.name
    }
}
