package com.redrield.frcscouting.elements.mutable

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.redrield.frcscouting.R
import com.redrield.frcscouting.elements.FormElementTextAreaFragment.Companion.TEXT_AREA_LABEL
import com.redrield.frcscouting.model.FragmentType
import com.redrield.frcscouting.model.PartialFragment
import kotlinx.android.synthetic.main.fragment_mutable_form_element_text_area.*

class MutableFormElementTextAreaFragment : MutableFormElementFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mutable_form_element_text_area, container, false)
    }

    override fun toImmutable(): PartialFragment {
        val args = Bundle().apply {
            putString(TEXT_AREA_LABEL, editTextLabel.text.toString())
        }

        return PartialFragment(FragmentType.TEXT_AREA, args)
    }

}