package com.redrield.frcscouting.elements.mutable

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.redrield.frcscouting.model.FragmentType
import com.redrield.frcscouting.model.PartialFragment
import com.redrield.frcscouting.R
import com.redrield.frcscouting.elements.PARAM_CHECKBOX_LABEL
import kotlinx.android.synthetic.main.fragment_mutable_form_element_checkbox.*

class MutableFormElementCheckboxFragment : MutableFormElementFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mutable_form_element_checkbox, container, false)
    }

    override fun toImmutable(): PartialFragment {
        val args = Bundle().apply {
            putString(PARAM_CHECKBOX_LABEL, checkBoxLabel.text.toString())
        }

        return PartialFragment(FragmentType.CHECK_BOX, args)
    }
}
