package com.redrield.frcscouting.elements

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.jakewharton.rxbinding2.widget.RxAdapterView
import com.redrield.frcscouting.R
import com.redrield.frcscouting.util.extensions.debounce
import com.redrield.frcscouting.util.extensions.ms
import kotlinx.android.synthetic.main.fragment_form_element_spinner.*

const val SPINNER_VALUES = "SPINNER_VALUES"
const val SPINNER_LABEL_TEXT = "SPINNER_LABEL_TEXT"

class FormElementSpinnerFragment : FormElementFragment() {

    lateinit var values: ArrayAdapter<String>
    lateinit var label: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            values = ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1).apply {
                addAll(it.getStringArrayList(SPINNER_VALUES)!!)
            }
            label = it.getString(SPINNER_LABEL_TEXT)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_element_spinner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spinnerLabel.text = label
        choiceSpinner.adapter = values

        // Put a default
        listener?.onMessage(FormElementMessage.Spinner(
                choiceSpinner.getItemAtPosition(0).toString(),
                label
        ))
        RxAdapterView.itemSelections(choiceSpinner)
                .debounce(300.ms)
                .subscribe {
                    listener?.onMessage(FormElementMessage.Spinner(
                            choiceSpinner.getItemAtPosition(it).toString(),
                            label))
                }
    }
}
