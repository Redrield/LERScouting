package com.redrield.frcscouting.elements.mutable

import android.support.v4.app.Fragment
import com.redrield.frcscouting.model.PartialFragment

/**
 * An abstract class for fragments that are being constructed in [com.redrield.frcscouting.NewFormElementActivity]
 */
abstract class MutableFormElementFragment : Fragment() {
    /**
     * Converts the state of this fragment into a [PartialFragment] that can be added to the [com.redrield.frcscouting.MainFragment]
     */
    abstract fun toImmutable(): PartialFragment
}