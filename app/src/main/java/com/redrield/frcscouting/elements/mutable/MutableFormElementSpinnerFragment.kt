package com.redrield.frcscouting.elements.mutable

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.redrield.frcscouting.R
import com.redrield.frcscouting.elements.SPINNER_LABEL_TEXT
import com.redrield.frcscouting.elements.SPINNER_VALUES
import com.redrield.frcscouting.model.FragmentType
import com.redrield.frcscouting.model.PartialFragment
import com.redrield.frcscouting.util.ListAdapter
import com.redrield.frcscouting.util.extensions.whenSelectionChanged
import kotlinx.android.synthetic.main.fragment_mutable_form_element_spinner.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.centerInParent
import org.jetbrains.anko.customView
import org.jetbrains.anko.dip
import org.jetbrains.anko.editText
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.noButton
import org.jetbrains.anko.padding
import org.jetbrains.anko.relativeLayout
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.yesButton

class MutableFormElementSpinnerFragment : MutableFormElementFragment() {
    lateinit var values: ListAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        values = ListAdapter<String>(context!!, android.R.layout.simple_list_item_1).apply {
            add("Select an Option")
            add("Add Item")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mutable_form_element_spinner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spinner.adapter = values
        spinner.whenSelectionChanged {
            when {
                it == "Add Item" -> {
                    val imm = ctx.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
                    context!!.alert {
                        lateinit var newItem: EditText
                        customView {
                            relativeLayout {
                                newItem = editText {
                                    hint = "Name"
                                }.lparams(width = matchParent) {
                                    centerInParent()
                                    padding = dip(10)
                                }
                            }
                        }

                        yesButton {
                            addItem(newItem.text.toString())
                            imm.hideSoftInputFromWindow(newItem.windowToken, 0)
                        }

                        noButton { }
                    }.show()

                }
                it != "Select an Option" -> {
                    context!!.alert {
                        message = "Are you sure you want to delete '$it'?"

                        yesButton { _ ->
                            removeItem(it)
                        }

                        noButton { }
                    }.show()
                }
            }

            spinner.setSelection(0)
        }
    }

    private fun addItem(item: String) {
        values.add(item)
    }

    private fun removeItem(item: String) {
        values.remove(item)
    }

    override fun toImmutable(): PartialFragment {
        val args = Bundle().apply {
            putStringArrayList(SPINNER_VALUES, ArrayList<String>(values.items).apply {
                // Remove the stuff we have there for our purposes
                removeAt(0)
                removeAt(0)
            })
            putString(SPINNER_LABEL_TEXT, labelText.text.toString())
        }

        return PartialFragment(FragmentType.SPINNER, args)
    }

    companion object {
        val TAG = MutableFormElementSpinnerFragment::class.java.name
    }
}
