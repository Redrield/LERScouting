package com.redrield.frcscouting.elements

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.widget.RxTextView
import com.redrield.frcscouting.R
import com.redrield.frcscouting.util.extensions.debounce
import com.redrield.frcscouting.util.extensions.ms
import kotlinx.android.synthetic.main.fragment_form_element_edit_text.*


/**
 * A finalized form element containing a labeled EditText for use in the scout's form
 */
class FormElementEditTextFragment : FormElementFragment() {
    lateinit var label: String
    var editTextType = -1

    // Fill our params using the fragment arguments
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            label = it.getString(EDIT_TEXT_LABEL)!!
            editTextType = it.getInt(EDIT_TEXT_TYPE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_element_edit_text, container, false)
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Makes UI mirror data from arguments
        labelView.text = label
        editText.inputType = editTextType

        // Send a default so that it gets included
        listener?.onMessage(FormElementMessage.EditText("", label))

        // Update listener with new edit text contents through this
        RxTextView.textChanges(editText)
            .debounce(500.ms)
            .filter(CharSequence::isNotEmpty)
            .subscribe {
                listener?.onMessage(FormElementMessage.EditText(it.toString(), label))
            }
    }

    companion object {
        const val EDIT_TEXT_LABEL = "EDIT_TEXT_LABEL"
        const val EDIT_TEXT_TYPE = "EDIT_TEXT_TYPE"
    }
}
