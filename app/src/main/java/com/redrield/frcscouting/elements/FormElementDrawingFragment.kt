package com.redrield.frcscouting.elements

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.jakewharton.rxbinding2.view.RxView
import com.redrield.frcscouting.R
import com.redrield.frcscouting.util.extensions.debounce
import com.redrield.frcscouting.util.extensions.ms
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_form_element_drawing.*
import org.jetbrains.anko.image

class FormElementDrawingFragment : FormElementFragment() {
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var lastX = 0f
    var lastY = 0f

    private lateinit var canvas: Canvas
    private lateinit var cleanBitmap: Bitmap
    var bmp = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
        set(value) {
            field = value
            fieldImageView.setImageBitmap(value)
            fieldImageView.invalidate()
        }

    lateinit var viewListener: Disposable

    init {
        paint.apply {
            color = Color.GREEN
            style = Paint.Style.STROKE
            strokeWidth = 10f
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_form_element_drawing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val bmp = (fieldImageView.image as BitmapDrawable).bitmap
        cleanBitmap = bmp
        this.bmp = bmp.copy(Bitmap.Config.ARGB_8888, true)
        canvas = Canvas(this.bmp)


        clearImageButton.setOnClickListener {
            clearView()
        }

        viewListener = RxView.touches(fieldImageView)
                .map { event ->
                    val (x, y) = mapCoords(fieldImageView, event)

                    listener?.onMessage(FormElementMessage.ReqDisallowScroll)

                    when (event.action) {
                        MotionEvent.ACTION_DOWN -> canvas.drawPoint(x, y, paint)
                        MotionEvent.ACTION_MOVE -> canvas.drawLine(lastX, lastY, x, y, paint)
                        MotionEvent.ACTION_UP -> listener?.onMessage(FormElementMessage.ReqAllowScroll)
                        else -> return@map event
                    }

                    fieldImageView.setImageBitmap(this.bmp)
                    fieldImageView.invalidate()
                    lastX = x
                    lastY = y
                    event
                }
                .debounce(500.ms)
                .subscribe {
                    Log.d(TAG, "Listener is $listener. Got subscription")
                    listener?.onMessage(FormElementMessage.FieldDiagram(this.bmp))
                }

        super.onViewCreated(view, savedInstanceState)
    }

    fun clearView() {
        bmp = cleanBitmap.copy(Bitmap.Config.ARGB_8888, true)
        canvas = Canvas(bmp)
    }

    companion object {
        val TAG = FormElementDrawingFragment::class.java.name
    }
}

/**
 * Copied from stackoverflow. Don't touch. Don't question. It holds this whole thing together
 *
 * I mean it, don't fucking touch
 */
fun mapCoords(view: ImageView, e: MotionEvent): Pair<Float, Float> {
    val idx = e.actionIndex
    val coords = floatArrayOf(e.getX(idx), e.getY(idx))
    val mat = Matrix()
    view.imageMatrix.invert(mat)
    mat.postTranslate(view.scrollX.toFloat(), view.scrollY.toFloat())
    mat.mapPoints(coords)

    return coords[0] to coords[1]
}