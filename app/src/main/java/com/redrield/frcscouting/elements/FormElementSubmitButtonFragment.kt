package com.redrield.frcscouting.elements

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.redrield.frcscouting.R
import kotlinx.android.synthetic.main.fragment_form_element_submit_button.*

// Has to be mutable for the structure to work. No mutable components actually container
class FormElementSubmitButtonFragment : FormElementFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_element_submit_button, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        submitButton.setOnClickListener {
            listener?.onMessage(FormElementMessage.Submit)
        }
    }
}
