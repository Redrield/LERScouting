package com.redrield.frcscouting.async

import com.google.android.gms.tasks.Task
import kotlin.coroutines.experimental.suspendCoroutine

suspend fun <T> Task<T>.result(): T = suspendCoroutine { cont ->
    addOnCompleteListener { cont.resume(it.result!!) }
    addOnFailureListener { cont.resumeWithException(it) }
}
