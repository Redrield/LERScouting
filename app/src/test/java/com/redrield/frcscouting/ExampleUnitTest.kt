package com.redrield.frcscouting

import org.junit.Test
import kotlin.system.measureNanoTime

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val list = listOf(
                listOf("a", "b", "c"),
                listOf("d", "e", "f")
        )

        val t1 = measureNanoTime {
            list
                    .map { it.toMutableList() }
                    .mapIndexed { idx, l ->
                        if (idx != list.lastIndex) {
                            println("first branch $l")
                            l.apply {
                                add("|")
                            }
                        } else {
                            println("Second branch $l")
                            l
                        }
                    }
        }
        println("Map time is $t1")

        val t2 = measureNanoTime {
            list.reduce { a, b -> a + "|" + b }
        }
        println("Reduce time is $t2")
    }
}
