package com.redrield.frcscouting

import android.os.Bundle
import android.support.test.runner.AndroidJUnit4
import com.redrield.frcscouting.util.extensions.eq
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val ba = Bundle().apply {
            putString("a", "b")
        }
        val bb = Bundle().apply {
            putString("a", "b")
        }
        assert(ba eq bb)
    }
}
